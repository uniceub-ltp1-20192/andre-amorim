#Centro Universitário de Brasília - UniCEUB
#Projeto Final LTP1 - Prof. Caio



#André Luís Amorim Deuschle RA: 21900962
#Gustavo Sousa de Lira RA: 21908311


from datetime import datetime
import random

all_products = [
    [1, 'Leite', 20, 3.00], 
    [2, 'Ovos', 30, 0.10], 
    [3, 'Farinha', 50, 3.00], 
    [4, 'Açúcar', 28, 2.00]
]

def menu():
    title(company_name)
    print('[1] Mostrar Lista de Produtos\n[2] Comprar\n[3] Vender\n[4] Adicionar Itens\n[5] Sair')

def display():
    print("ID\tProduto\t\t\t\tQuantidade\tPreço")
    for item in all_products:
        print(f'{item[0]}\t{item[1]}\t\t\t\t{item[2]}\t\tR${item[3]:.2f} ')

def order_summary(product):
    total = float(item[3]) * qtd
    print(f'\nOrdem de compra - Data:{str(datetime.now())}\nEmpresa: {company_name.upper()}\nNome do produto: {item[1]}\nPreço: R${item[3]:.2f}\nTotal a ser pago: R${total:.2f}')

def title(text):
    print('*'*len(text)*2)
    print(text.center(len(text)*2,' '))
    print('*'*len(text)*2)

print('>>>>> GERENCIADOR DE ESTOQUE <<<<<')
company_name = input('Nome da sua empresa: ').title()
if company_name == '':
    company_name = 'Mercadinho'

menu()

while True:
    choice = input('>>> ')
    print('')
    
    if choice == '1': display()

    elif choice == '2':
        print('COMPRAR ITEM')
        prod_id = int(input(f'ID do produto(1-{len(all_products)}): '))
        for item in all_products:
            if item[0] == prod_id:
                qtd = int(input('Quantidade: '))
                order_summary(item)
                cnf = input("Confirmar compra(S/N): ").upper()
                if cnf == 'S':
                    item[2] += qtd
                    print('COMPRA REALIZADA COM SUCESSO!')
                else:
                    print("Continuar comprando")

    elif choice == '3':
        print('VENDER ITEM')
        prod_id = int(input(f'ID do produto(1-{len(all_products)}): '))
        for item in all_products:
            if item[0] == prod_id:
                qtd = int(input('Quantidade: '))
                item[2] -= qtd
                print('VENDA REALIZADA COM SUCESSO!')

    elif choice == '4':
        print('ADICIONAR ITEM')
        prod = []
        prod.append(len(all_products)+1)
        prod.append(input("Nome do produto: ").title())
        prod.append(int(input("Disponível: ")))
        prod.append(float(input("Preço: ")))
        all_products.append(prod)

    elif choice == '5':
        print('Fim do Programa')
        break