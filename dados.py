from datetime import datetime
import random

class Dados:
  def listaDeItens(obj):
    print("ID\tProduto\t\t\t\tQuantidade\tPreço")
    for item in obj:
        print(f'{item[0]}\t{item[1]}\t\t\t\t{item[2]}\t\tR${item[3]:.2f} ')
  
  def comprarItem(obj, name):
    def order_summary(product):
      total = float(item[3]) * qtd
      print(f'\nOrdem de compra - Data:{str(datetime.now())}\nEmpresa: {name.upper()}\nNome do produto: {item[1]}\nPreço: R${item[3]:.2f}\nTotal a ser pago: R${total:.2f}')
    print('COMPRAR ITEM')
    prod_id = int(input(f'ID do produto(1-{len(obj)}): '))
    for item in obj:
        if item[0] == prod_id:
            qtd = int(input('Quantidade: '))
            order_summary(item)
            cnf = input("Confirmar compra(S/N): ").upper()
            if cnf == 'S':
                item[2] += qtd
                print('COMPRA REALIZADA COM SUCESSO!')
            else:
                print("Continuar comprando")

  def venderItem(obj):
    print('VENDER ITEM')
    prod_id = int(input(f'ID do produto(1-{len(obj)}): '))
    for item in obj:
        if item[0] == prod_id:
            qtd = int(input('Quantidade: '))
            item[2] -= qtd
            if item[2] < 0:
              item[2] = item[2] * 0
            print('VENDA REALIZADA COM SUCESSO!')

  def adicionarItem(obj):
    print('ADICIONAR ITEM')
    prod = []
    prod.append(len(obj)+1)
    prod.append(input("Nome do produto: ").title())
    prod.append(int(input("Disponível: ")))
    prod.append(float(input("Preço: ")))
    obj.append(prod)
    print('ITEM ADICIONADO COM SUCESSO!')