#Centro Universitário de Brasília - UniCEUB
#Projeto Final LTP1 - Prof. Caio

#André Luís Amorim Deuschle RA: 21900962
#Gustavo Sousa de Lira RA: 21908311

from Menu.menu import Menu
from Dados.dados import Dados
from Validador.validador import Validador

produtos = [
      [1, 'Leite', 20, 3.00], 
      [2, 'Ovos', 30, 0.10], 
      [3, 'Farinha', 50, 3.00], 
      [4, 'Açúcar', 28, 2.00]
  ]

company_name = Menu.menuEmpresa()
Menu.menuPrincipal(company_name)

while True:
    choice = Validador.validar('[1-5]', 'Opção Inválida - Deve estar entre {}')
    
    if choice == '1': Dados.listaDeItens(produtos)
    elif choice == '2': Dados.comprarItem(produtos, company_name)
    elif choice == '3': Dados.venderItem(produtos)
    elif choice == '4': Dados.adicionarItem(produtos)
    elif choice == '5': break